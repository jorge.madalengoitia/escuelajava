package com.everis.apirest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.CustomerReducidoResource;
import com.everis.apirest.controller.resource.CustomerResource;
import com.everis.apirest.controller.resource.ProductoResource;
import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.service.CustomerService;
import com.everis.apirest.service.ProductoService;

@RestController
public class ApiController {
	
	@Autowired
	ProductoService productoService;
	
	@Autowired
	CustomerService customerService;
	
	@Value("${igv}")
	String igv;
	
	@GetMapping("/igv")
	public String igv() {
		return igv;
	}
	
	@GetMapping("/productos")
	@RequestMapping(path = "/productos", method = RequestMethod.GET)
	public List<ProductoResource> obtenerProductos() {
		
		List<ProductoResource> listado = new ArrayList<>();
		productoService.obtenerProductos().forEach(producto -> {
			ProductoResource productoResource = new ProductoResource();
			productoResource.setId(producto.getId());
			productoResource.setNombre(producto.getName());
			productoResource.setDescripcion(producto.getDescription());
			listado.add(productoResource);
		});
		return listado;
	}
	
	@GetMapping("/productos/{id}") //http://localhost:9090/productos/2 -->200(OK) 404 (Recurso no encontrado) - 500 (Internal Error)
	public ProductoResource obtenerProductoPorId(@PathVariable("id") Long id) throws Exception {
		Producto producto = productoService.obtenerProductoPorId(id);
		ProductoResource productoResource = new ProductoResource();
		productoResource.setId(producto.getId());
		productoResource.setNombre(producto.getName());
		productoResource.setDescripcion(producto.getDescription());
		return productoResource;
	}
	
	@GetMapping("/customers")
	public List<CustomerResource> obtenerClientes() {
		
		List<CustomerResource> listado = new ArrayList<>();
		customerService.obtenerClientes().forEach(cliente -> {
			CustomerResource customerResource = new CustomerResource();
			customerResource.setId(cliente.getId());
			customerResource.setNombre(cliente.getName());
			customerResource.setApellidoCompleto(cliente.getLastName() + " " + cliente.getLastName2());
			listado.add(customerResource);
		});
		return listado;
	}
	
	@PostMapping("/customers")
	public CustomerResource guardarCliente(@RequestBody CustomerReducidoResource request) {
		Customer nuevo = new Customer();
		nuevo.setName(request.getNombre());
		nuevo.setLastName(request.getApellidoCompleto());
		Customer customer = customerService.guardar(nuevo);
		CustomerResource response = new CustomerResource();
		response.setId(customer.getId());
		response.setNombre(customer.getName());
		response.setApellidoCompleto(customer.getLastName() + " " + customer.getLastName2());
		return response;
	}

}
