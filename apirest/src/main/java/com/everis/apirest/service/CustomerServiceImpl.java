package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.model.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Iterable<Customer> obtenerClientes() {
		return customerRepository.findAll();
	}

	@Override
	public Customer guardar(Customer customer) {
		return customerRepository.save(customer);
	}
	
	
}
