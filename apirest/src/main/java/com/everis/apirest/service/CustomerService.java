package com.everis.apirest.service;

import com.everis.apirest.model.entity.Customer;

public interface CustomerService {
	
	public Iterable<Customer> obtenerClientes();
	
	public Customer guardar(Customer customer);
	
}
